//
// Created by colin on 5/19/20.
//

#ifndef RESULT_UTILS_H
#define RESULT_UTILS_H

#include <string>
#include "result/Result.hpp"

using division_error = std::string;
const std::string div_by_zero_err = "can't divide by zero!";

template <typename T>
using division_result = result::Result<T, division_error>;

auto safe_divide_int(int x, int y) -> result::Result<int, division_error>
{
    if (y != 0)
        return result::Result<int, std::string>::make_ok(x / y);

    return result::Result<int, std::string>::make_err(div_by_zero_err);
}

#endif //RESULT_UTILS_H
