//
// Created by colin on 5/19/20.
//
#include <string>
#include "bandit/bandit.h"
#include "result/Result.hpp"
#include "utils.h"

using namespace snowhouse;
using namespace bandit;
using namespace result;

using std::string;

go_bandit([]() {
    describe("", []() {
        it("safely divides by zero", []() {
            auto res = safe_divide_int(1, 0);
            AssertThat(res.get_ok(), IsNull());
            AssertThat(*(res.get_err()), Equals(div_by_zero_err));
        });
    });
    describe("status checks", []() {
        Result<int, string> resOk {"uninitialzed"};
        Result<int, string> resErr {"uninitialzed"};

        before_each([&]() {
            resOk = 0;
            resErr = "error";
        });

        it("is_ok with ok vals", [&]() {
            AssertThat(resOk.is_ok(), IsTrue());
        });
        it("is_ok with err vals", [&]() {
            AssertThat(resErr.is_ok(), IsFalse());
        });
        it("is_err with ok vals", [&]() {
            AssertThat(resOk.is_err(), IsFalse());
        });
        it("is_err with err vals", [&]() {
            AssertThat(resErr.is_err(), IsTrue());
        });
    });
});


int main(int argc, char* argv[])
{
    return bandit::run(argc, argv);
}