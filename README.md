This repo contains a header-only implementation of a `Result<OkType, ErrType` template class
which represents the result of a computation. If the computation is successful, it will hold
an `OkType`, and if there are any errors it will hold an `ErrType`. It's mostly a wrapper
around `std::variant` that adds features such as mapping over the result and chaining
computations together.