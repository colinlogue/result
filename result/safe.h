//
// Created by colin on 5/19/20.
//

#ifndef RESULT_SAFE_H
#define RESULT_SAFE_H

#include <functional>

#include "result/Result.hpp"


namespace result
{
    template <typename OkType, typename ErrType, typename ...Args>
    auto make_safe(std::function<OkType(Args...)> f, std::function<ErrType(std::exception&)> to_err)
        -> std::function<Result<OkType, ErrType>(Args...)>
    {
        return [f, to_err](Args...args) -> Result<OkType, ErrType> {
            try
            {
                return Result<OkType, ErrType>::make_ok(f(args...));
            }
            catch (std::exception& e)
            {
                return Result<OkType, ErrType>::make_err(to_err(e));
            }
        };
    }

    template <typename OkType, typename ...Args>
    auto with_err_msg(std::function<OkType(Args...)> f, const std::string& msg)
        -> std::function<Result<OkType, std::string>(Args...)>
    {
        return [f, msg](Args...args) -> Result<OkType, std::string> {
            try
            {
                return Result<OkType, std::string>::make_ok(f(args...));
            }
            catch (...)
            {
                return Result<OkType, std::string>::make_err(msg);
            }
        };
    }
}

#endif //RESULT_SAFE_H
